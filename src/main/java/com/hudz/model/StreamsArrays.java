package com.hudz.model;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsArrays {

    public List<Integer> getRandomIntegers(int size) {
        final Random rnd = new Random();
        List<Integer> listInt = Stream.generate(() -> rnd.nextInt(100))
                .limit(size)
                .collect(Collectors.toList());
        return listInt;
    }

    public IntSummaryStatistics intSummaryStatistics (List<Integer> integerList) {
        IntSummaryStatistics statistics = integerList.stream()
                .mapToInt(Integer::intValue)
                .summaryStatistics();
        return statistics;
    }

    public double countAvgUSingStream(List<Integer> integerList) {
        OptionalDouble avg = integerList.stream().mapToInt(e -> e).average();
        if (avg.isPresent()) {
            return avg.getAsDouble();
        }
        return -1;
    }

    public int countMin(List<Integer> integerList) {
        int min = intSummaryStatistics(integerList).getMin();
        return min;
    }

    public int countMax(List<Integer> integerList) {
        int max = intSummaryStatistics(integerList).getMax();
        return max;
    }
    public int countSum(List<Integer> integerList) {
        int sum = integerList.stream().mapToInt(e -> e).sum();
        return sum;
    }
    public int countSumReduce(List<Integer> integerList) {
        OptionalInt sum = integerList.stream().mapToInt(e -> e).reduce((a, b) -> a + b);
        if (sum.isPresent()) {
            return sum.getAsInt();
        }
        return -1;
    }



}
