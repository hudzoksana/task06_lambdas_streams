package com.hudz.model.lambdas.func;

import java.util.Arrays;

public class ExampleLambda {

    public void getMaxValueLambdas() {
        MyLambda maxValue = (a,b,c) -> Math.max(Math.max(a,b),c);
        int[] array = {1, 4 ,5};
        System.out.println("Our array: " + Arrays.toString(array) +
                "\nMax = " + maxValue.myFunc(array[0],array[1],array[2]));
    }

    public void getAverageValue() {
        MyLambda averageValue = ((a, b, c) -> (a + b + c) / 3);
        int[] array = {1, 4 ,5};
        System.out.println("Our array: " + Arrays.toString(array) +
                "\nAVG = " + averageValue.myFunc(array[0],array[1],array[2]));
    }

}
