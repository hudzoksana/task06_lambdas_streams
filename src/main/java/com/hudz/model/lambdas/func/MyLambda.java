package com.hudz.model.lambdas.func;

public interface MyLambda {

    int myFunc(int a, int b, int c);
}
