package com.hudz.model;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringApp {

    private List<String> strings;

    public StringApp() {
        this.strings = null;
    }
    public StringApp(List<String> s) {
        this.strings = s;
    }

    public long getCountUnique(List<String> stringArrays) {
        final long count = stringArrays.stream().distinct().count();
        return count;
    }
    public List<String> getSortedStrings(List<String> stringArrays) {
        return stringArrays.stream().sorted().distinct().collect(Collectors.toList());
    }

    public Map getWordCount(List<String> stringArrays) {
        Map<String, Integer> countWords = new HashMap<>();
        int count = 0;
        for (String s:stringArrays) {
            count = 0;
            for (String o: stringArrays) {
                if (s.equals(o)) {
                    count++;
                }
            }
            countWords.put(s,count);
        }
        return countWords;
    }

    public Map getSymbolsCount(List<String> stringArrays) {
        Map<String, Integer> countWords = new HashMap<>();
        int count = 0;

        List<String> symbols = stringArrays.stream()
                .flatMap(e -> Stream.of(e.split("")))
                .collect(Collectors.toList());
        Iterator<String> iterator = symbols.iterator();

        while (iterator.hasNext()) {
            String str = iterator.next();
            if (!str.equals(str.toLowerCase())) {
                iterator.remove();
            }
        }


        for (String s : symbols) {
            count = 0;
            for (String o : symbols) {
                if (s.equals(o)) {
                    count++;
                }
            }
            countWords.put(s,count);
        }
        return countWords;
    }
}
