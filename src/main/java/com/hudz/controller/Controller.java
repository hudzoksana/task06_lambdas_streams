package com.hudz.controller;

import java.util.List;
import java.util.Map;

public interface Controller {

    long numberQnique(List<String> stringArrays);

    List<String> getSortedStrings(List<String> stringArrays);

    Map getWordCount(List<String> stringArrays);

    Map getOccurance(List<String> stringArrays);

    List<Integer> controlGetRandomInteger(int size);

    double countAverage(List<Integer> stringArrays);

    int getMin(List<Integer> integersArrays);

    int getMax(List<Integer> integersArrays);

    int getSum(List<Integer> integersArrays);

    int getSumReduce(List<Integer> integersArrays);

    void countAverageLambda();

    void getMaxLambda();

    String getCommandsPattern();
}
