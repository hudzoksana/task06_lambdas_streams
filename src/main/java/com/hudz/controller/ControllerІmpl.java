package com.hudz.controller;

import com.hudz.model.*;
import com.hudz.model.lambdas.command.Command;
import com.hudz.model.lambdas.command.CommandImpl;
import com.hudz.model.lambdas.func.ExampleLambda;
import java.util.List;
import java.util.Map;

public class ControllerІmpl implements Controller {
  private Model model;
  private StringApp stringApp;
  private StreamsArrays streamsArrays;
  private ExampleLambda exampleLambda;
  private CommandImpl command;

  public ControllerІmpl() {
    stringApp = new StringApp();
    streamsArrays = new StreamsArrays();
    exampleLambda = new ExampleLambda();
    command = new CommandImpl();
  }
  @Override
  public long numberQnique(List<String> stringArrays) {

      long count = stringApp.getCountUnique(stringArrays);
      return count;
  }

  @Override
  public List<String> getSortedStrings(List<String> stringArrays) {
    return stringApp.getSortedStrings(stringArrays);
  }

  @Override
  public Map getWordCount(List<String> stringArrays) {
    return stringApp.getWordCount(stringArrays);
  }

  @Override
  public Map getOccurance(List<String> stringArrays) {
    return stringApp.getSymbolsCount(stringArrays);
  }

  @Override
  public List<Integer> controlGetRandomInteger(int size) {
    return streamsArrays.getRandomIntegers(size);
  }

  @Override
  public double countAverage(List<Integer> integerList) {
      return streamsArrays.countAvgUSingStream(integerList);
  }

  @Override
  public int getMin(List<Integer> integersArrays) {
    return streamsArrays.countMin(integersArrays);
  }

  @Override
  public int getMax(List<Integer> integersArrays) {
    return streamsArrays.countMax(integersArrays);
  }

  @Override
  public int getSum(List<Integer> integersArrays) {
    return streamsArrays.countSum(integersArrays);
  }

  @Override
  public int getSumReduce(List<Integer> integersArrays) {
    return streamsArrays.countSumReduce(integersArrays);
  }

  @Override
  public void countAverageLambda() {
      exampleLambda.getAverageValue();
  }

  @Override
  public void getMaxLambda() {
      exampleLambda.getMaxValueLambdas();
  }

  @Override
  public String getCommandsPattern() {
    return command.execute("string");
  }
}
