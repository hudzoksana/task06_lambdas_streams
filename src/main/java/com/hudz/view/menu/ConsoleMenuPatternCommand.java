package com.hudz.view.menu;

import java.util.LinkedHashMap;

public class ConsoleMenuPatternCommand extends ConsoleMenu {
    public ConsoleMenuPatternCommand() {
        this.menu = new LinkedHashMap<>();
        this.menu.put("1", " 1 - Print 4 commands");
        this.menu.put("Q", "  Q - exit");
        this.methodsMenu.put("1",this::getCommands);
    }

    private void getCommands() {
        System.out.println("Nothing there");
//        System.out.println(controller.getCommandsPattern());
    }

}
