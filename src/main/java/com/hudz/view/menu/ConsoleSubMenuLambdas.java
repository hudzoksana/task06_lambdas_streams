package com.hudz.view.menu;

import java.util.LinkedHashMap;

public class ConsoleSubMenuLambdas extends ConsoleMenu {

    public ConsoleSubMenuLambdas() {
        this.menu = new LinkedHashMap<>();
        this.menu.put("1", " 1 - Get max value with lambdas");
        this.menu.put("2", " 2 - Count average with lambdas");
        this.menu.put("Q", "  Q - exit");
        this.methodsMenu.put("1",this::getMaxLambda);
        this.methodsMenu.put("2",this::countAverageLambda);
    }

    private void countAverageLambda() {
        controller.countAverageLambda();
    }

    private void getMaxLambda() {
        controller.getMaxLambda();
    }
}
