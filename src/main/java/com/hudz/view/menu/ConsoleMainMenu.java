package com.hudz.view.menu;

public class ConsoleMainMenu extends ConsoleMenu {

  ConsoleSubMenuStreams consoleSubMenuStreams;
  ConsoleSubMenuStrings consoleSubMenuStrings;
  ConsoleSubMenuLambdas consoleSubMenuLambdas;
  ConsoleMenuPatternCommand consoleMenuPatternCommand;

  public ConsoleMainMenu() {

    consoleSubMenuStreams = new ConsoleSubMenuStreams();
    consoleSubMenuStrings = new ConsoleSubMenuStrings();
    consoleSubMenuLambdas = new ConsoleSubMenuLambdas();
    consoleMenuPatternCommand = new ConsoleMenuPatternCommand();
    menu.put("1", "  1 - Operations with strings");
    menu.put("2", "  2 - Operations with streams");
    menu.put("3", "  3 - Pattern Command");
    menu.put("4", "  4 - Lambda Functions");
    menu.put("Q", "  Q - exit");

    methodsMenu.put("1", consoleSubMenuStrings::appString);
    methodsMenu.put("2", consoleSubMenuStreams::show);
    methodsMenu.put("3", consoleMenuPatternCommand::show);
    methodsMenu.put("4", consoleSubMenuLambdas::show);
//    methodsMenu.put("4", this::print);
//    methodsMenu.put("5", this::getSize);
  }
}
