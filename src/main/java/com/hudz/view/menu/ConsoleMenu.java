package com.hudz.view.menu;

import com.hudz.controller.Controller;
import com.hudz.controller.ControllerІmpl;
import com.hudz.view.Printable;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public abstract class ConsoleMenu {

    protected Controller controller;
    protected Map<String, String> menu;
    protected Map<String, Printable> methodsMenu;
    protected Scanner input;
    ConsoleMenu() {
        controller = new ControllerІmpl();
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
    }
    protected void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : this.menu.values()) {
            System.out.println(str);
        }
    }
    public void show() {
        String keySubMenu;
        do {
            this.outputMenu();
            System.out.println("Please, select menu point.");
            keySubMenu = input.nextLine().toUpperCase();

            try {
                this.methodsMenu.get(keySubMenu).print();
            } catch (Exception e) {
                System.out.println("not correct");
            }
        } while (!keySubMenu.equals("Q"));
    }
}
