package com.hudz.view.menu;

import java.util.*;

public class ConsoleSubMenuStreams extends ConsoleMenu {

    List<Integer> integersArrays;

    public ConsoleSubMenuStreams() {
        this.integersArrays = new ArrayList<>();
        this.menu = new LinkedHashMap<>();
        this.menu.put("1", " 1 - Get random integers");
        this.menu.put("2", " 2 - Count average");
        this.menu.put("3", " 3 - Count min and max");
        this.menu.put("4", " 4 - Sum of values");
        this.menu.put("Q", "  Q - exit");
        this.methodsMenu.put("1",this::getRandomIteger);
        this.methodsMenu.put("2",this::countAverage);
        this.methodsMenu.put("3",this::countMinMax);
        this.methodsMenu.put("4",this::getSumOfValues);
    }


    private void getRandomIteger() {
        //TODO: user input size
        integersArrays = this.controller.controlGetRandomInteger(5);
        System.out.println(integersArrays);
    }
    private void getSumOfValues() {
        System.out.println("Sum of values using sum()= " + this.controller.getSum(integersArrays)
                + "\nSum of values using reduce() = " + this.controller.getSumReduce(integersArrays));
    }

    private void countMinMax() {
        System.out.println("Min = " + controller.getMin(integersArrays)
                + " Max = " + controller.getMax(integersArrays));
    }

    private void countAverage() {
        System.out.println(controller.countAverage(integersArrays));
    }

    public void appStreamsArray() {
        this.show();
    }
}
