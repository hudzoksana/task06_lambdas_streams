package com.hudz.view.menu;

import java.util.*;

public class ConsoleSubMenuStrings extends ConsoleMenu {

    List<String> stringArrays;

    public ConsoleSubMenuStrings() {
        this.stringArrays = new ArrayList<>();
        this.menu.put("1", " 1 - Number of unique words");
        this.menu.put("2", " 2 - Sorted list of unique words");
        this.menu.put("3", " 3 - Word count");
        this.menu.put("4", " 4 - Occurrence number");
        this.menu.put("Q", "  Q - exit");
        this.methodsMenu.put("1", this::getNumbeUnique);
        this.methodsMenu.put("2", this::getSortedList);
        this.methodsMenu.put("3", this::getWordCount);
        this.methodsMenu.put("4", this::getOccurance);
    }

    private void getOccurance() {
        System.out.println("Occurrence number of each symbol except upper case characters is:" + controller.getOccurance(stringArrays));
    }

    private void getWordCount() {
        System.out.println("In list are:\n" + controller.getWordCount(stringArrays));
    }

    private void getSortedList() {
        System.out.println("Sorted list: " + controller.getSortedStrings(stringArrays));
    }

    private void getNumbeUnique() {
        System.out.println("Number of unique words in list:" + controller.numberQnique(stringArrays));
    }

    public void appString() {
        System.out.println("Input strings:");
        String check;
        while (input.hasNextLine()) {
            try {
                check = input.nextLine();
                if (!check.isEmpty()) {
                    stringArrays.add(check);
                } else break;
            } catch (Exception e) {
            }
        }
        if (stringArrays.isEmpty()) {

        } else this.show();
    }
}
